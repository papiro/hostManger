'use strict'

let http = require('http')
,   util = require('util')
,   pa = require('parseargv')

,   args = pa(process.argv)

,   hm = http.createServer((req, res) => {

  let vhost = req.headers.host.split(':').shift() // port strip

  if (args.hasOwnProperty(vhost)) {
    
  } else {
    console.log('${vhost} not included in host map')
    process.exit(1)
})

hm.listen(args.port, () => {
  console.log('listening on ${args.port}')
})
